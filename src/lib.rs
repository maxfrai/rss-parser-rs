// #![feature(test)]
// extern crate test;

use url::Url;

use chrono::{DateTime, NaiveDateTime, TimeZone, Utc};
use slug::slugify;
use std::{borrow::Cow, cell::RefCell};

use failure::Fail;

use xml::attribute::OwnedAttribute;
use xml::reader::{EventReader, Events, XmlEvent};

#[derive(Fail, Debug)]
pub enum RssParseError {
    #[fail(display = "")]
    Placeholder,
}

#[derive(Debug)]
pub enum ParseMode {
    All,
    Latest(usize),
}

pub trait RssItem {
    fn set_link(&mut self, link: Url);
    fn link(&self) -> Option<&Url>;

    fn set_title(&mut self, title: &str);
    fn title(&self) -> Option<&String>;

    fn set_slug(&mut self, slug: &str);
    fn slug(&self) -> Option<&String>;

    fn set_description(&mut self, desc: &str);
    fn description(&self) -> Option<&String>;

    fn set_enclosure(&mut self, data: &str);
    fn enclosure(&self) -> Option<&String>;

    fn set_pub_date(&mut self, date: DateTime<Utc>);
    fn pub_date(&self) -> Option<&DateTime<Utc>>;

    fn set_category(&mut self, category: &str);
    fn category(&self) -> Option<&String>;
}

pub struct RssProcessor<T: RssItem + Default + Sized + Clone> {
    parse_mode: ParseMode,
    current_element: RefCell<String>,
    current_element_attributes: RefCell<Vec<OwnedAttribute>>,
    items: RefCell<Vec<T>>,
}

#[derive(Debug, Clone)]
pub struct RssItemDefault {
    pub link: Option<Url>,
    pub title: Option<String>,
    pub slug: Option<String>,
    pub pub_date: Option<DateTime<Utc>>,
    pub description: Option<String>,
    pub enclosure: Option<String>,
    pub category: Option<String>,
}

impl Default for RssItemDefault {
    fn default() -> Self {
        Self {
            link: None,
            title: None,
            slug: None,
            pub_date: None,
            description: None,
            enclosure: None,
            category: None,
        }
    }
}

impl RssItem for RssItemDefault {
    fn set_link(&mut self, link: Url) {
        self.link = Some(link);
    }

    fn link(&self) -> Option<&Url> {
        self.link.as_ref()
    }

    fn set_title(&mut self, title: &str) {
        self.title = Some(title.to_owned());
    }
    fn title(&self) -> Option<&String> {
        self.title.as_ref()
    }

    fn set_slug(&mut self, slug: &str) {
        self.slug = Some(slug.to_owned());
    }
    fn slug(&self) -> Option<&String> {
        self.slug.as_ref()
    }

    fn set_description(&mut self, desc: &str) {
        self.description = Some(desc.to_owned());
    }
    fn description(&self) -> Option<&String> {
        self.description.as_ref()
    }

    fn set_enclosure(&mut self, data: &str) {
        self.enclosure = Some(data.to_owned());
    }
    fn enclosure(&self) -> Option<&String> {
        self.enclosure.as_ref()
    }

    fn set_pub_date(&mut self, date: DateTime<Utc>) {
        self.pub_date = Some(date);
    }
    fn pub_date(&self) -> Option<&DateTime<Utc>> {
        self.pub_date.as_ref()
    }

    fn set_category(&mut self, category: &str) {
        self.category = Some(category.to_owned());
    }
    fn category(&self) -> Option<&String> {
        self.category.as_ref()
    }
}

impl<T: RssItem + Default + Sized + Clone> RssProcessor<T> {
    pub fn new(parse_mode: ParseMode) -> Self {
        RssProcessor {
            parse_mode,
            current_element: RefCell::new(String::default()),
            current_element_attributes: RefCell::new(Vec::new()),
            items: RefCell::new(Vec::default()),
        }
    }

    fn reset_inner(&self) {
        let mut items = self.items.borrow_mut();
        let mut current_element = self.current_element.borrow_mut();

        (*items).clear();
        (*current_element) = String::default();
    }

    fn parse_start_element(&self, element_name: &str, attributes: Vec<OwnedAttribute>) -> bool {
        let mut items = self.items.borrow_mut();
        let mut current_element = self.current_element.borrow_mut();
        let mut current_element_attributes = self.current_element_attributes.borrow_mut();

        if element_name == "item" || element_name == "entry" {
            if let ParseMode::Latest(max) = self.parse_mode {
                if items.len() >= max {
                    return false;
                }
            }

            (*items).push(T::default());
        }

        *current_element_attributes = attributes;
        *current_element = element_name.to_string();

        if element_name == "enclosure" {
            if let Some(item) = (*items).last_mut() {
                for attribute in &*current_element_attributes {
                    if attribute.name.local_name == "url" {
                        item.set_enclosure(&attribute.value);
                    }
                }
            }
        }

        true
    }

    fn parse_characters(&self, mut text: &str) {
        let mut items = self.items.borrow_mut();
        let current_element = self.current_element.borrow();
        let current_element_attributes = self.current_element_attributes.borrow();

        if let Some(item) = (*items).last_mut() {
            match (*current_element).as_ref() {
                "link" | "feedburner:origLink" | "id" => {
                    if let Ok(url) = Url::parse(text) {
                        if url.host_str().is_some() && text.find("|").is_none() {
                            item.set_link(url);
                        }
                    }
                }
                "title" => {
                    if item.title().is_none() {
                        item.set_title(text);
                        item.set_slug(&slugify(text));
                    }
                }
                "enclosure" => {}
                "category" => item.set_category(text),
                "description" | "summary" => item.set_description(text),
                "pubDate" | "dc:date" | "updated" => {
                    text = text.trim();

                    let date = if let Ok(date) = text.parse::<DateTime<Utc>>() {
                        Some(date)
                    } else if let Ok(date) = DateTime::parse_from_rfc2822(text) {
                        Some(date.with_timezone(&Utc))
                    } else if let Ok(date) = DateTime::parse_from_rfc3339(text) {
                        Some(date.with_timezone(&Utc))
                    } else {
                        let variants = [
                            "%a, %d %b %Y %T %z",
                            "%a, %d %b %Y %T Z",
                            "%a, %d %b %Y %T CET",
                            "%m/%d/%Y %r",
                            "%Y-%m-%d %T",
                            "%d %b %y %H:%M UTC",
                            "%Y-%m-%d",
                        ];
                        let mut possible_date: Option<DateTime<Utc>> = None;
                        for variant in &variants {
                            if let Ok(naive) = NaiveDateTime::parse_from_str(text, variant) {
                                possible_date = Some(Utc.from_utc_datetime(&naive));
                                break;
                            };
                        }

                        possible_date
                    };

                    if let Some(date_u) = date {
                        item.set_pub_date(date_u);
                    } else {
                        // println!("Failed to parse date: {}", text);
                    }
                }
                _ => {}
            }
        }
    }

    fn parse_events<R: std::io::Read>(&self, parser: Events<R>) -> Result<(), RssParseError> {
        for event in parser {
            // println!("EVENT: {:?}", event);
            match event {
                Ok(XmlEvent::StartElement {
                    name, attributes, ..
                }) => {
                    if !self.parse_start_element(&name.local_name, attributes) {
                        break;
                    }
                }
                Ok(XmlEvent::Characters(text)) | Ok(XmlEvent::CData(text)) => {
                    self.parse_characters(&text);
                }
                Err(_) => {
                    // println!("{:?}", e);
                }
                _ => {}
            }
        }

        Ok(())
    }

    fn validate<'a, S: Into<Cow<'a, str>>>(&self, xml: S) -> Cow<'a, str> {
        let mut data = xml.into();

        // Remove any data until first <
        if let Some(index) = data.find("<") {
            if index != 0 {
                data.to_mut().drain(0..index);
            }
        }

        data
    }

    pub fn process(&self, xml: &str) -> Result<Vec<T>, RssParseError> {
        self.reset_inner();

        let validated = self.validate(xml);
        let xml_parser = EventReader::new(validated.as_bytes());
        self.parse_events(xml_parser.into_iter())?;
        Ok(self.items.borrow().to_vec())
    }
}

// #[cfg(test)]
// mod tests {
//     use crate::{ParseMode, RssItem, RssItemDefault, RssProcessor};
//     use browser_rs::Browser;
//     use url::Url;

//     #[test]
//     pub fn test_my() {
//         let xml = "
//             <?xml version=\"1.0\" encoding=\"UTF-8\" ?>
//             <rss version=\"2.0\">
//                 <channel>
//                     <title>W3Schools Home Page</title>
//                     <link>https://www.w3schools.com</link>
//                     <description>Free web building tutorials</description>
//                     <item>
//                         <title>RSS Tutorial</title>
//                         <link>https://www.w3schools.com/xml/xml_rss.asp</link>
//                         <description>New RSS tutorial on W3Schools</description>
//                         <pubDate>2019-03-05</pubDate>
//                     </item>
//                 </channel>
//             </rss>
//             ";

//         {
//             let processor = RssProcessor::<RssItemDefault>::new(ParseMode::Latest(1));
//             let items = processor.process(xml);
//             println!("{:#?}", items);
//         }
//     }

//     #[test]
//     pub fn test_with_browser() {
//         use std::time::Duration;

//         let url = Url::parse("http://www.dailynk.com/rsskor/rss").unwrap();
//         let browser = Browser::new("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36", Duration::from_secs(7));
//         let response = browser.get(url).unwrap();

//         // println!("{:?}", response.data);

//         let processor = RssProcessor::<RssItemDefault>::new(ParseMode::All);
//         let items = processor.process(&response.data).unwrap();

//         println!("Num itms: {}", items.len());
//         println!("FIRST:\n{:?}", items.get(0));

//         println!("-------------");

//         for item in items {
//             if item.pub_date.is_none() || item.title.is_none() {
//                 println!("NONE ITEM:\n{:?}", item);
//             }
//         }

//         assert_eq!(false, true);
//     }

//     #[test]
//     pub fn test() {
//         let xml = "
//             <?xml version=\"1.0\" encoding=\"UTF-8\" ?>
//             <rss version=\"2.0\">
//                 <channel>
//                     <title>W3Schools Home Page</title>
//                     <link>https://www.w3schools.com</link>
//                     <description>Free web building tutorials</description>
//                     <item>
//                         <title>RSS Tutorial</title>
//                         <link>https://www.w3schools.com/xml/xml_rss.asp</link>
//                         <description>New RSS tutorial on W3Schools</description>
//                         <pubDate>Tue, 26 Oct 2004 14:06:44 -0500</pubDate>
//                     </item>
//                     <item>
//                         <title>XML Tutorial</title>
//                         <link>https://www.w3schools.com/xml</link>
//                         <description>New XML tutorial on W3Schools</description>
//                         <pubDate>Wed, 27 Oct 2004 14:06:44 -0500</pubDate>
//                     </item>
//                 </channel>
//             </rss>
//             ";

//         {
//             let processor = RssProcessor::<RssItemDefault>::new(ParseMode::Latest(1));
//             let items = processor.process(xml);
//             assert_eq!(items.is_ok(), true);
//             assert_eq!(items.as_ref().unwrap().len(), 1);
//             assert_eq!(
//                 items.as_ref().unwrap().first().unwrap().title(),
//                 Some(&"RSS Tutorial".to_owned())
//             );

//             println!("{:#?}", items);
//         }

//         {
//             let processor = RssProcessor::<RssItemDefault>::new(ParseMode::Latest(2));
//             let items = processor.process(xml);
//             assert_eq!(items.is_ok(), true);
//             assert_eq!(items.as_ref().unwrap().len(), 2);
//             assert_eq!(
//                 items.as_ref().unwrap().first().unwrap().title(),
//                 Some(&"RSS Tutorial".to_owned())
//             );
//             assert_eq!(
//                 items.as_ref().unwrap().get(1).unwrap().title(),
//                 Some(&"XML Tutorial".to_owned())
//             );

//             println!("{:#?}", items);
//         }

//         {
//             let processor = RssProcessor::<RssItemDefault>::new(ParseMode::All);
//             let items = processor.process(xml);
//             assert_eq!(items.is_ok(), true);
//             assert_eq!(items.as_ref().unwrap().len(), 2);
//         }
//     }
// }
